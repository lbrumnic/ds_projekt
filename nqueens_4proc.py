"""
Program za generiranje svih mogucih kombinacija
slaganja 8 kraljica na sahovsku plocu

Ovaj program pokrece se u dva procesa

Prosjecno vrijeme izvodjenja programa ovom metodom je:
0.0907 sekundi
"""

from mpi4py import MPI
from itertools import permutations
import numpy as np
import time

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

a=0
n = 8
cols = range(n)

def conflict( vec ):
	"""
	Provjerava je li primljeni vektor jedno od mogucih rjesenja
	problema 8 kraljica

	Argumenti:
	vec -- vektor koji predstavlja jednu od permutacija brojeva
	0, 1, 2, 3, 4, 5, 6 i 7

	Vraca:
	True ukoliko je vektor zadovoljio uvjet da predstalvlja jedno
	od rjesenja 8 kraljica
	"""
    if n == len(set(vec[i]+i for i in cols)) \
            == len(set(vec[i]-i for i in cols)):
        
        print ( vec )
        return( True ) 

permutation_items = []
permutationss = permutations(cols, n)
permutation_item = list(permutationss)

cetvrtina = round(len(permutation_item)/4)
pola = cetvrtina*2
tricetri = cetvrtina*3
duljina = round(len(permutation_item))

start = time.time()

if rank == 0:
    for vec in permutation_item[0:cetvrtina]:
        if (conflict( vec ) == True):
			a+=1
if rank ==1:
    for vec in permutation_item[cetvrtina:pola]:
        if (conflict( vec ) == True):
			a+=1

if rank == 2:
    for vec in permutation_item[pola:tricetri]:
        if (conflict( vec ) == True):
			a+=1

if rank == 3:
    for vec in permutation_item[tricetri:duljina]:
        if (conflict( vec ) == True):
			a+=1
            print ( vec )
			
end = time.time()

print("Proces ranka", rank, "retultati:", a, "Vrijeme: ", end-start)

