"""
Program za generiranje svih mogucih kombinacija
slaganja 8 kraljica na sahovsku plocu

Ovaj program pokrece se u jednom procesu

Prosjecno vrijeme izvodjenja programa ovom metodom je:
0.19385 sekundi
"""

from itertools import permutations
import numpy as np
import time

a=0		#brojac
n=8		#broj kraljica

def conflict( vec ):
	"""
	Provjerava je li primljeni vektor jedno od mogucih rjesenja
	problema 8 kraljica

	Argumenti:
	vec -- vektor koji predstavlja jednu od permutacija brojeva
	0, 1, 2, 3, 4, 5, 6 i 7

	Vraca:
	True ukoliko je vektor zadovoljio uvjet da predstalvlja jedno
	od rjesenja 8 kraljica
	"""
    if n == len(set(vec[i]+i for i in cols)) \
            == len(set(vec[i]-i for i in cols)):
        
        print ( vec )
        return( True ) 

cols = range(n)


permutation_items = []
permutationss = permutations(cols, n)
permutation_item = list(permutationss)

poladuljine = round(len(permutation_item)/2)
duljina = round(len(permutation_item))

start = time.time()

for vec in permutation_item:
    if (conflict( vec )==True):
        a+=1
		
end = time.time()

print("Broj retultata:", a, "Vrijeme: ", end-start)